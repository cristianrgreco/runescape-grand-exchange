import fs from 'fs';
import path from 'path';
import ItemWrapper from './item-wrapper';

describe('ItemWrapper', () => {

  it('should return information about the item', () => {
    const mockSource = fs.readFileSync(path.join(__dirname, '../test-fixtures/sample_santa-hat.html'), 'utf-8');

    const itemWrapper = new ItemWrapper(mockSource);

    itemWrapper.getName().should.equal('Santa hat');
    itemWrapper.getPrice().should.equal('364,790,871');
    itemWrapper.getDescription().should.equal(`It's a Santa hat.`);
    itemWrapper.getImageUrl().should.equal('https://vignette2.wikia.nocookie.net/runescape2/images/4/47/Santa_hat.png/revision/latest?cb=20160502072108');
  });
});
