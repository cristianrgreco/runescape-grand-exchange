import { http }  from 'follow-redirects';
import log4js from 'log4js';

const logger = log4js.getLogger();

const options = {
  host: 'runescape.wikia.com',
  port: 80,
  path: '/wiki'
};

export default function grandExchangeQuery(name) {
  const options = optionsForItem(encodeName(name));
  logger.debug(`fetchItem(${name}) with options: ${JSON.stringify(options)}`);

  return new Promise((resolve, reject) => {
    http.get(options, res => {
      const chunks = [];
      res.on('data', chunk => chunks.push(chunk));
      res.on('end', () => resolve(chunks.join('')));
    }).on('error', error => logger.error(`fetchItem(${name}) failed: ${error}`));
  });
}

function encodeName(name) {
  return name.replace(/ /g, '_').toLowerCase();
}

function optionsForItem(name) {
  return Object.assign({}, options, {path: `${options.path}/${name}`});
}
