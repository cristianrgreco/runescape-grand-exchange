import fs from 'fs';
import path from 'path';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import nock from 'nock';
import grandExchangeQuery from './grand-exchange-client';

chai.use(chaiAsPromised);
const should = chai.should();

describe('GrandExchangeClient', () => {

  it('should return the HTML source for a given item name', () => {
    const mockResponse = fs.readFileSync(path.join(__dirname, '../test-fixtures/sample_santa-hat.html'), 'utf-8');

    const scope = nock('http://runescape.wikia.com')
      .get('/wiki/santa_hat')
      .reply(200, mockResponse);

    grandExchangeQuery('SANTA HAT').should.eventually.equal(mockResponse);
    scope.isDone().should.be.true;
  });
});
