import { JSDOM } from 'jsdom';

export default class ItemWrapper {

  constructor(source) {
    const document = new JSDOM(source).window.document;

    this.name = document.querySelector('.wikitable > caption').textContent;
    this.price = document.querySelector('.wikitable .infobox-quantity .infobox-quantity-replace').textContent;
    this.description = document.querySelector('.wikitable a[title=Examine]').parentNode.nextSibling.textContent;
    this.imageUrl = document.querySelector('.wikitable .infobox-image .image img').getAttribute('src');
  }

  getName() {
    return this.name;
  }

  getPrice() {
    return this.price;
  }

  getDescription() {
    return this.description;
  }

  getImageUrl() {
    return this.imageUrl;
  }
}
