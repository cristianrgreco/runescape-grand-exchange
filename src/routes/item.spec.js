import fs from 'fs';
import path from 'path';
import chai from 'chai';
import chaiHttp from 'chai-http';
import nock from 'nock';
import { app } from '../index';

chai.use(chaiHttp);
const should = chai.should();

describe('Items', () => {

  describe('/GET/:name items', () => {

    it('should GET an item by the given name', done => {
      const mockResponse = fs.readFileSync(path.join(__dirname, '../test-fixtures/sample_santa-hat.html'), 'utf-8');
      nock('http://runescape.wikia.com')
        .get('/wiki/santa_hat')
        .reply(200, mockResponse);

      chai.request(app)
        .get('/items/santa%20hat')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name').that.equal('Santa hat');
          res.body.should.have.property('price').that.equal('364,790,871');
          res.body.should.have.property('description').that.equal(`It's a Santa hat.`);
          res.body.should.have.property('imageUrl').that.equal('https://vignette2.wikia.nocookie.net/runescape2/images/4/47/Santa_hat.png/revision/latest?cb=20160502072108');
          done();
        });
    });
  });
});
