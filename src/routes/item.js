import log4js from 'log4js';
import grandExchangeQuery from '../clients/grand-exchange-client';
import ItemWrapper from '../clients/item-wrapper';

const logger = log4js.getLogger();

export function getItem(req, res) {
  const name = req.params.name;
  logger.info(`GET /items/${name}`);

  grandExchangeQuery(name)
    .then(source => res.json(new ItemWrapper(source)))
    .catch(error => res.send(error));
}
