import express from 'express';
import log4js from 'log4js';
import { getItem } from './routes/item';

export const app = express();
export const port = 3000;

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.route('/items/:name')
  .get(getItem);

app.listen(3000, () => log4js.getLogger().info(`Listening on port ${port}`));
