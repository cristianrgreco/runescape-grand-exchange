FROM node:8.1.2

WORKDIR /src

COPY . .
RUN npm i

EXPOSE 3000

CMD ["npm", "start"]
